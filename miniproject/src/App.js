import './css/style.css';
import Content from './Component/Content';
import Footer from './Component/Footer';
import Head from './Component/Head';

function App() {
  return (
    <div>
      
      <Head />
      <Content />
      <Footer />
    </div>
  );
}

export default App;
