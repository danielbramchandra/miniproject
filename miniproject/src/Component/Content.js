import React, { Component } from 'react'
import Articles from './Content/Articles';
import ImageData from './Content/ImageContent';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import ReactPaginate from 'react-paginate';
class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            img: [],
            article: []
        }

    }

    componentDidMount() {
        fetch("http://localhost:8080/image/getall").then((res) =>
            res.json()
        )
            .then((json) => {
                this.setState({
                    img: json,
                });
            }
            );

        fetch("http://localhost:8080/content/getall").then((res) =>
            res.json()
        )
            .then((json) => {
                this.setState({
                    article: json,
                });
            }
            );

    }

    render() {

        return (
            <div>
                <div id="test">
                    <h1>HOT SHIRTS FOR THIS MONTH</h1>
                    <ul>

                        <Carousel
                            additionalTransfrom={0}
                            arrows={true}
                            autoPlay
                            autoPlaySpeed={2000}
                            centerMode={false}
                            className=""
                            containerClass="container-with-dots"
                            dotListClass=""
                            draggable={false}
                            focusOnSelect={true}
                            infinite
                            itemClass=""
                            keyBoardControl
                            minimumTouchDrag={80}
                            renderButtonGroupOutside={false}
                            renderDotsOutside={false}
                            responsive={{
                                desktop: {
                                    breakpoint: {
                                        max: 3000,
                                        min: 1024
                                    },
                                    items: 3,
                                    partialVisibilityGutter: 40
                                },
                                mobile: {
                                    breakpoint: {
                                        max: 464,
                                        min: 0
                                    },
                                    items: 1,
                                    partialVisibilityGutter: 30
                                },
                                tablet: {
                                    breakpoint: {
                                        max: 1024,
                                        min: 464
                                    },
                                    items: 2,
                                    partialVisibilityGutter: 30
                                }
                            }}
                            showDots={true}
                            sliderClass=""
                            slidesToSlide={1}
                            swipeable
                        >
                            {
                                this.state.img.map((data) => {
                                    return (

                                        <ImageData imgUrl={data.imgUrl} />
                                    )
                                })
                            }
                        </Carousel>


                    </ul>
                </div>
                <div id="body">
                    <div id="contents">
                        <div id="articles">
                            <Carousel
                                additionalTransfrom={0}
                                arrows={true}
                                centerMode={false}
                                className=""
                                containerClass="container-with-dots"
                                dotListClass=""
                                draggable={false}
                                focusOnSelect={true}
                                itemClass=""
                                keyBoardControl
                                minimumTouchDrag={80}
                                renderButtonGroupOutside={false}
                                renderDotsOutside={false}
                                responsive={{
                                    desktop: {
                                        breakpoint: {
                                            max: 3000,
                                            min: 1024
                                        },
                                        items: 3,
                                        partialVisibilityGutter: 40
                                    },
                                    mobile: {
                                        breakpoint: {
                                            max: 464,
                                            min: 0
                                        },
                                        items: 1,
                                        partialVisibilityGutter: 30
                                    },
                                    tablet: {
                                        breakpoint: {
                                            max: 1024,
                                            min: 464
                                        },
                                        items: 2,
                                        partialVisibilityGutter: 30
                                    }
                                }}                                
                                sliderClass=""
                                slidesToSlide={1}
                                swipeable
                            >
                                {
                                    this.state.article.map((data) => {
                                        return (<Articles artHeader={data.title} artContent={data.description} artButton={data.action} />)

                                    })
                                }
                            </Carousel>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}
export default Content;