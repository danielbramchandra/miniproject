import React, { Component } from 'react'

class Articles extends Component {
    render() {
        return (
            <li>
                <h1>{this.props.artHeader}</h1>
                <p>{this.props.artContent}</p>
                <div class="more">{this.props.artButton}</div>
            </li>
        )
    }
}
export default Articles;