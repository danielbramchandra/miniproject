import React, { Component } from 'react'

class Footer extends Component {
    render() {
        return (
            <footer id="footer">
                <div class="background">
                    <div class="body">
                        <div>
                            <h3>GET WEEKLY NEWSLETTER</h3>
                            <div class="subscribe">
                                <form>
                                    <input class="txtfield" type="text"></input>
                                    <input class="button" type="button"></input>
                                </form>
                            </div>
                        </div>

                        <div>

                            <div class="posts">
                                <h3>LATEST POST</h3>
                                <p><a href="">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nihil, vero temporibus
                                est dolorem impedit animi aperiam dignissimos error. </a><span>TEST</span></p>
                            </div>
                        </div>
                        <div>
                            <h3>FOLLOW US</h3>
                            <div class="connect">
                                <a class="facebook"></a>
                                <a class="twitter"></a>
                                <a class="googleplus"></a>
                            </div>
                        </div>

                    </div>
                </div>
            </footer>
        )
    }
}
export default Footer;