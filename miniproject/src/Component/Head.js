import React, { Component } from 'react'
import Navigation from './Header/Navigation'

class Head extends Component {
    render() {
        return (
            <header id="header">
                <div>
                    <Navigation />
                </div>
            </header>
        )
    }
}
export default Head;
