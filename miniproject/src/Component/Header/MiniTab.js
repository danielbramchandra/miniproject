import React, { Component } from 'react'

class MiniTab extends Component {
    render() {
        return (
            <div className={this.props.classElement}>
                <a href="/">{this.props.text1}</a>
                <a href="/">{this.props.text2}</a>
            </div>
        )
    }
}
export default MiniTab;
