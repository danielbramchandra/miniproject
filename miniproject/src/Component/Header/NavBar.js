import React, { Component } from 'react'

class NavBar extends Component {
    render() {
        return (
            <ul id={this.props.id}>
                <li><a href=""><span>{this.props.text1}</span></a></li>
                <li><a href=""><span>{this.props.text2}</span></a></li>
                <li><a href=""><span>{this.props.text3}</span></a></li>
            </ul>
        )
    }
}
export default NavBar;