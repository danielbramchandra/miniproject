import React, { Component } from 'react'
import MiniTab from './MiniTab'
import NavBar from './NavBar'

class Navigation extends Component {
    render() {
        return (
            <div id="navigation">
                <MiniTab classElement="infos" text1="Cart" text2="0 Items" />
                <MiniTab text1="Login" text2="Register" />
                <NavBar id="primary" text1="Home" text2="About" text3="Men" />
                <img id="logo" src="./images/logo.png" alt=""></img>
                <NavBar id="secondary" text1="Women" text2="Blog" text3="Content" />
            </div>
        )
    }
}
export default Navigation;