import React, { Component } from 'react'

class DBServices extends Component {
    get(mapping) {
        return fetch("http://localhost:8080/" + mapping + "/getall")
    }
    save(mapping) {
        let data =
        {
            page: document.getElementById("pg").value,
            type: document.getElementById("type").value,
            imgUrl: document.getElementById("url").value

        }
            ;
        fetch("http://localhost:8080/" + mapping + "/add", {
            // Adding method type
            method: "POST",

            // Adding body or contents to send
            body: JSON.stringify(data),

            // Adding headers to the request
            headers: {
                "Content-type": "application/json; charset=UTF-8",
            },
        });

    }
}
export default DBServices;