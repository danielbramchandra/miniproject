package id.co.nexsoft.miniprojectapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniprojectapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniprojectapiApplication.class, args);
	}

}
