package id.co.nexsoft.miniprojectapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.miniprojectapi.model.ContentCollection;
import id.co.nexsoft.miniprojectapi.repository.ContentCollectionRepository;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/content")
public class ContentController {
    @Autowired
    ContentCollectionRepository repo;
    
    @PostMapping(value = "/add")
    public void add(ContentCollection content){
        repo.save(content);
    }
    @GetMapping(value = "/getall")
    public ArrayList<ContentCollection> getAll(){        
        return (ArrayList<ContentCollection>) repo.findAll();
    }
}