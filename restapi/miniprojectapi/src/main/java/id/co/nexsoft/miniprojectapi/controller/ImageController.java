package id.co.nexsoft.miniprojectapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.miniprojectapi.model.ImageCollection;
import id.co.nexsoft.miniprojectapi.repository.ImageCollectionRepository;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/image")
public class ImageController {
    @Autowired
    ImageCollectionRepository repo;

    @PostMapping(value = "/add")
    public void add(ImageCollection image) {
        repo.save(image);
    }

    @GetMapping(value = "/getall")
    public ArrayList<ImageCollection> getAll() {
        return (ArrayList<ImageCollection>) repo.findAll();
    }
}
