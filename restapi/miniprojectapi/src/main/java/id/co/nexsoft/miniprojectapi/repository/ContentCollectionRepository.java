package id.co.nexsoft.miniprojectapi.repository;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.miniprojectapi.model.ContentCollection;

public interface ContentCollectionRepository extends CrudRepository<ContentCollection,Integer> {
    
}
