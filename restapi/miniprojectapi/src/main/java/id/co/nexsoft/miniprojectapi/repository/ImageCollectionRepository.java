package id.co.nexsoft.miniprojectapi.repository;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.miniprojectapi.model.ImageCollection;

public interface ImageCollectionRepository extends CrudRepository<ImageCollection,Integer>{

}
    

